

const rimraf = require('./rimraf');

// Parse command-line arguments
const args = process.argv.slice(2);
const command = args[0];
const options = args.slice(1);

// Handle rimraf command
if (command === 'rimraf') {
  const pathIndex = options.findIndex(option => option === '--path');

  if (pathIndex === -1 || pathIndex === options.length - 1) {
    throw new Error('Invalid command. Please provide the --path option.');
  }

  const folderPath = options[pathIndex + 1];

  rimraf(folderPath, (error) => {
    if (error) {
      console.error('An error occurred while removing the folder:', error);
    } else {
      console.log('Folder removed successfully.');
    }
  });
} else {
  throw new Error('Invalid command. Please provide a valid command.');
}



